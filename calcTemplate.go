// +build ignore

package main

//Calculate : calculate things
func Calculate(n1, n2 int, symbol rune) string {
	if symbol == '*' {
	{{.Multiply}}
	} else if symbol == '/' {
	{{.Divide}}
	} else if symbol == '+' {
	{{.Add}}
	} else if symbol == '-' {
	{{.Subtract}}
	}
	return "Not implemented!"
}

func main() {
	println(Calculate(34, 25, '/'))
}